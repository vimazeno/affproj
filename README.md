# affProj

Un projet pour montrer comment freezer une lib obsolète, pour continuer d'utiliser un binaire en le distribuant via docker

`affProj` est un binaire écrit il y a longtemps par des étudiants permettant de d'affecter des étudiants sur des projets en tenant compte de leurs voeux priorisés (programmation sous contrainte).

`affProj` utilise la librairie `libglpk-dev` ... mais une vielle version, dont un .so a été sauvé de feu `front` ... poauh

Comme il a été validé que copier cette lib dans `/usr/lib/` sur une xenial permet de faire fonctionner `affProj`.

L'idée est ici de "containeriser" la vieille lib et le binaire dans une xenial et de l'exposer.

## utilisation local

```shell
$ sudo apt install -y libglpk-dev
$ sudo cp libglpk.so.0 /usr/lib/
# importer le dump SQL d'intranet en local
$ mkdir /tmp/csv
$ sudo chmod -R 777 /tmp/csv/
$ mysql -u admin intranet < export_projetsZZ1.sql
$ rm -rf csv
$ mv /tmp/csv .
$ ./affProj -v csv/voeux.csv -b csv/binomes.csv -p csv/projets.csv -s
```

### épuration de l'output

remplacer

* `, sent)` par `)`
* `, 0)` par `);`

la colonne sent n'existe plus ET il faut un ; à la fin de chaque instruction



## utilisation docker

* tirer l'image xenial et construire le container

```
bin/setup
```

* lancer `affProj` dans le contexte du projets

```
bin/activate
affProj
```

* utilisation de affProj dans la vraie vie

```
affProj -v csv/voeux.csv -b csv/binomes.csv -p csv/projets.csv -s
```

## explorer le container docker

* se connecter en ssh à la machine docker

```
sudo docker exec -i -t zz1projet /bin/bash
```

* lancer affProj "manuellement"

```
sudo docker exec -i -t zz1projet /bin/bash -c /app/affProj
```
