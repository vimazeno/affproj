# Use an official Python runtime as a parent image
FROM ubuntu:16.04

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN apt-get update
RUN apt-get install -y libglpk-dev
RUN cp libglpk.so.0 /usr/lib/
RUN chmod 777 /app/affProj

# Run app.py when the container launches
CMD ["/bin/bash"]
