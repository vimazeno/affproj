#
# EXPORT DES TABLES POUR ATTRIBUTION DES PROJETS ZZ1
# 20150202:GMA:creation
#

# RECUPERATION DES BINOMES AVEC SUBSTITUTION DES ID PAR NOMS ET PRENOMS
# CSV SEPARE PAR ";"
# EXPORT DU FICHIER AU FORMAT UNIX
SELECT 'id','nom1','prenom1','nom2','prenom2','nom3','prenom3'
UNION
SELECT id,
       u.user_lastname, u.user_firstname,
      uu.user_lastname, uu.user_firstname,
      '', ''
 FROM projet_binome
 INNER JOIN projet_users AS u
  ON u.user_id = user1
 INNER JOIN projet_users AS uu
  ON uu.user_id = user2
UNION
SELECT id,
       u.user_lastname, u.user_firstname,
      uu.user_lastname, uu.user_firstname,
     uuu.user_lastname, uuu.user_firstname
 FROM projet_binome
 INNER JOIN projet_users AS u
  ON u.user_id = user1
 INNER JOIN projet_users AS uu
  ON uu.user_id = user2
 INNER JOIN projet_users AS uuu
  ON uuu.user_id = user3
 INTO OUTFILE '/tmp/csv/binomes.csv'
  FIELDS TERMINATED BY ';' ENCLOSED BY '"'
  LINES TERMINATED BY '\n' ;

# RECUPERATION DES SUJETS AVEC SUBSTITUTION DES AUTEURS PAR NOMS ET PRENOMS
# CSV SEPARE PAR ";"
# EXPORT DU FICHIER AU FORMAT DOS
SELECT 'id','title','user_lastname'
UNION
SELECT id, title, user_lastname
 FROM projet_sujet
 INNER JOIN projet_users
  ON user_id = auteur_id
 INTO OUTFILE '/tmp/csv/projets.csv'
  FIELDS TERMINATED BY ';' ENCLOSED BY '"'
  LINES TERMINATED BY '\r\n' ;

# RECUPERATION DES SUJETS AVEC SUBSTITUTION DES AUTEURS PAR NOMS ET PRENOMS
# CSV SEPARE PAR ";"
# EXPORT DU FICHIER AU FORMAT DOS
SELECT 'id','title','user_firstname', 'user_lastname'
UNION
SELECT id, title, user_firstname, user_lastname
 FROM projet_sujet
 INNER JOIN projet_users
  ON user_id = auteur_id
 INTO OUTFILE '/tmp/csv/projets_with_name.csv'
  FIELDS TERMINATED BY ';' ENCLOSED BY '"'
  LINES TERMINATED BY '\r\n' ;


# RECUPERATION DES VOEUX
# CSV SEPARE PAR ";"
# EXPORT DU FICHIER AU FORMAT DOS
SELECT 'id_binome','id_sujet1','id_sujet2','id_sujet3'
UNION
SELECT id_binome,id_sujet1,id_sujet2,id_sujet3
 FROM projet_voeux
 INTO OUTFILE '/tmp/csv/voeux.csv'
  FIELDS TERMINATED BY ';' ENCLOSED BY '"'
  LINES TERMINATED BY '\r\n' ;
